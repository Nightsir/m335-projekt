var menuPlay = (function()
{
	function init()
	{
		adaptSize();
		setNumberOfGhosts(3);
	}
	
	function adaptSize()
	{
		var height = window.innerHeight;
		var width = window.innerWidth;
		
		var menu = document.getElementById('menu');
		var header = document.getElementById('header');
		var content = document.getElementById('content');
		var titleTextH1 = document.getElementById('titleText').getElementsByTagName('h1')[0];
		var titleTextH3 = document.getElementById('titleText').getElementsByTagName('h3')[0];
		var menuNavigation = document.getElementById('navigationPlay');
		var menuNavigationChooserLinks = menuNavigation.getElementsByTagName('ul')[0].getElementsByTagName('a');
		var menuNavigationMenuLinks = menuNavigation.getElementsByTagName('ul')[1].getElementsByTagName('a');
		var backButton = document.getElementById('back');
		var playButton = document.getElementById('play');
		
		menu.style.height = height + 'px';
		header.style.height = height * 0.38 + "px";
		content.style.height = height * 0.62 + "px";
		
		if(height < width)
		{
			titleTextH1.style.fontSize = height * 0.12 + "px";
			titleTextH1.style.paddingTop = height * 0.035 + "px";
			titleTextH3.style.fontSize = height * 0.05 + "px"
			
			backButton.style.fontSize = height * 0.051 + "px";
			
			menuNavigation.style.fontSize = height * 0.06 + "px";
			menuNavigation.getElementsByTagName('ul')[1].style.marginTop = height * 0.08 + "px";
		}
		else
		{
			titleTextH1.style.fontSize = width * 0.12 + "px";
			titleTextH1.style.paddingTop = width * 0.035 + "px";
			titleTextH3.style.fontSize = width * 0.05 + "px"
			
			backButton.style.fontSize = width * 0.051 + "px";
			
			menuNavigation.style.fontSize = width * 0.06 + "px";
			menuNavigation.getElementsByTagName('ul')[1].style.marginTop = width * 0.08 + "px";
		}
		
		forEach.call(menuNavigationChooserLinks, function(e)
		{
			e.style.marginBottom = height * 0.02 + "px";
			e.style.paddingTop = height * 0.027 + "px";
			e.style.paddingBottom = height * 0.027 + "px";
			e.style.marginTop = height * 0.02 + "px";
			e.style.width = e.offsetHeight + "px";
		});
		
		forEach.call(menuNavigationMenuLinks, function(e)
		{
			e.style.marginTop = height * 0.1 + "px";
			
			e.style.paddingTop = height * 0.03 + "px";
			e.style.paddingBottom = height * 0.03 + "px";
		});
		
		menuNavigation.style.width = width * 0.75 + "px";
		menuNavigation.style.paddingLeft = ((width - menuNavigation.style.width.replace("px", "")) / 2) + "px";
		
		playButton.style.width = width * 0.58 + "px";
		backButton.style.width = width * 0.13 + "px";
	}
	
	function setNewNumberOfGhosts(element)
	{
		var menuNavigationChooserLinks = document.getElementById('navigationPlay').getElementsByTagName('a');
		forEach.call(menuNavigationChooserLinks, function(e)
		{
			e.className = e.className.replace(/ selected/, '');
		});
		
		element.className += " selected";
		
		data.sNumberOfGhosts(element.innerHTML);
	}
	
	function setNumberOfGhosts(number)
	{
		var storagedNumber = data.gNumberOfGhosts();

		var menuNavigationChooserLinks = document.getElementById('navigationPlay').getElementsByTagName('a');
		console.log(storagedNumber);
		if(storagedNumber == null)
		{
			data.sNumberOfGhosts(number);
			menuNavigationChooserLinks[number-1].className += " selected";
			return;
		}
		
		forEach.call(menuNavigationChooserLinks, function(e)
		{			
			if(e.innerHTML == storagedNumber)
			{
				e.className += " selected";
			}
		});
	}
	
	return {init: init, adaptSize : adaptSize, setNewNumberOfGhosts : setNewNumberOfGhosts};
})();

window.onload = menuPlay.init;
window.onresize = menuPlay.adaptSize;