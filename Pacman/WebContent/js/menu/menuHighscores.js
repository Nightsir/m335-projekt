var menuHighscores = (function()
{
	function init()
	{
		adaptSize();
		tabbingPrepare();
	}
	
	function adaptSize()
	{
		var height = window.innerHeight;
		var width = window.innerWidth;
		
		var menu = document.getElementById('menu');
		var header = document.getElementById('header');
		var content = document.getElementById('content');
		var titleTextH1 = document.getElementById('titleText').getElementsByTagName('h1')[0];
		var titleTextH3 = document.getElementById('titleText').getElementsByTagName('h3')[0];
		var menuNavigation = document.getElementById('navigationHighscores');
		var menuNavigationMenuLinks = menuNavigation.getElementsByTagName('ul')[1].getElementsByTagName('a');
		var backButton = document.getElementById('back');
		var tabcontainer = document.getElementById('tabContainer');
		var tabscontent = document.getElementById('tabscontent');
		var tabs = document.getElementById('tabs').getElementsByTagName('ul')[0].children;
		
		menu.style.height = height + 'px';
		header.style.height = height * 0.25 + "px";
		content.style.height = height * 0.75 + "px";
		
		if(height < width)
		{
			titleTextH1.style.fontSize = height * 0.12 + "px";
			titleTextH1.style.paddingTop = height * 0.035 + "px";
			titleTextH3.style.fontSize = height * 0.05 + "px"
			
			backButton.style.fontSize = height * 0.051 + "px";
			
			menuNavigation.style.fontSize = height * 0.06 + "px";
			menuNavigation.getElementsByTagName('ul')[1].style.marginTop = height * 0.08 + "px";
		}
		else
		{
			titleTextH1.style.fontSize = width * 0.12 + "px";
			titleTextH1.style.paddingTop = width * 0.035 + "px";
			titleTextH3.style.fontSize = width * 0.05 + "px"
			
			backButton.style.fontSize = width * 0.051 + "px";
			
			menuNavigation.style.fontSize = width * 0.06 + "px";
			menuNavigation.getElementsByTagName('ul')[1].style.marginTop = width * 0.08 + "px";
		}
		
		forEach.call(menuNavigationMenuLinks, function(e)
		{
			e.style.marginBottom = height * 0.02 + "px";
			e.style.paddingTop = height * 0.03 + "px";
			e.style.paddingBottom = height * 0.03 + "px";
		});
		
		menuNavigation.style.width = width * 0.75 + "px";
		menuNavigation.style.paddingLeft = ((width - menuNavigation.style.width.replace("px", "")) / 2) + "px";
		
		backButton.style.width = width * 0.13 + "px";
		
		tabcontainer.style.width = width * 0.75 + "px";
		
		var ctr = 0;
		var containerWidth = tabcontainer.style.width;
		
		forEach.call(tabs, function(e)
		{
			e.style.paddingBottom = height * 0.02 + "px";
			e.style.paddingTop = height * 0.02 + "px";
			
			var unsizedTabWidth = e.offsetWidth;
			var sizeBetweenTabs = width * 0.01;
			
			e.style.width = ((containerWidth.replace("px", "") - 3 * sizeBetweenTabs) / 4) + "px";
			
			if(ctr != 0)
			{
				e.style.marginLeft = sizeBetweenTabs + "px";
			}
			
			ctr += 1;
		});
		
		tabscontent.style.height = height * 0.3 + "px";
		tabscontent.style.paddingTop = height * 0.025 + "px";
		tabscontent.style.paddingLeft = width * 0.025 + "px";
		tabscontent.style.paddingRight = width * 0.025 + "px";
		tabscontent.style.paddingBottom = height * 0.02 + "px";
		tabscontent.style.fontSize = height * 0.05 + "px";
	}
	
	function tabbingPrepare()
	{
		// get tab container
	  	var container = document.getElementById("tabContainer");
	  	var tabcon = document.getElementById("tabscontent");
	  	
	  	var storagedNumberOfGhosts = data.gNumberOfGhosts();
	  	var navitem;
	  	
	  	if(storagedNumberOfGhosts != null)
		{
	  		navitem = document.getElementById("tabHeader_" + storagedNumberOfGhosts);
		}
	  	else
  		{
		    navitem = document.getElementById("tabHeader_3");
  		}

	  	// store which tab we are on
	    var ident = navitem.id.split("_")[1];
	    navitem.parentNode.setAttribute("data-current", ident);
	    
	    // set current tab with class of activetabheader
	    navitem.setAttribute("class", "tabActiveHeader");
	    
	    // hide tab contents we don't need
	   	var pages = tabcon.getElementsByTagName("div");
		for (var i = 0; i < pages.length; i++) 
		{
			if(i != storagedNumberOfGhosts * 1 - 1)
			{
				pages.item(i).style.display = "none";
			}
			else
			{
				var highscores = getHighscoresForNumberOfGhosts(i + 1);
				
				if(highscores == null)
		    	{
					var tabpage = document.getElementById('tabpage_' + (i + 1));
					tabpage.style.display = "block";
				    var noHighscoresElement = document.createElement("p");
					noHighscoresElement.innerHTML = "There are no highscores yet...";
					tabpage.appendChild(noHighscoresElement);
					continue;
		    	}
				
				for(var x = 0; x < 5; x++)
				{
					var scoreElement = document.createElement("p");
					scoreElement.innerHTML = highscores[x];
					pages.item(i).appendChild(scoreElement);
				}
			}
		};
		
		if(storagedNumberOfGhosts == null)
    	{
			var tabpage = document.getElementById('tabpage_3');
			tabpage.style.display = "block";
		    var noHighscoresElement = document.createElement("p");
			noHighscoresElement.innerHTML = "There are no highscores yet...";
			tabpage.appendChild(noHighscoresElement);
    	}

	    // this adds click event to tabs
	    var tabs = container.getElementsByTagName("li");
	    for (var i = 0; i < tabs.length; i++) 
	    {
	    	tabs[i].onclick = tabbingClick;
	    }
	}
	
	function tabbingClick() 
	{
		var current = this.parentNode.getAttribute("data-current");
		
		// remove class of activetabheader and hide old contents
		document.getElementById("tabHeader_" + current).removeAttribute("class");
	  	document.getElementById("tabpage_" + current).style.display = "none";

	  	var ident = this.id.split("_")[1];
	  	var tabpage = document.getElementById("tabpage_" + ident);
		var highscores = getHighscoresForNumberOfGhosts(ident);
		
		while (tabpage.firstChild)
		{
			tabpage.removeChild(tabpage.firstChild);
		}
		
		if(highscores == null)
		{
			var noHighscoresElement = document.createElement("p");
			noHighscoresElement.innerHTML = "There are no highscores yet...";
			tabpage.appendChild(noHighscoresElement);
		}
		else
		{
			for(var i = 0; i < 5; i++)
			{
				var scoreElement = document.createElement("p");
				scoreElement.innerHTML = highscores[i];
				tabpage.appendChild(scoreElement);
			}
		}
		
	  	// add class of activetabheader to new active tab and show contents
	  	this.setAttribute("class" ,"tabActiveHeader");
	  	tabpage.style.display = "block"
	  	this.parentNode.setAttribute("data-current", ident);
	}
	
	function getHighscoresForNumberOfGhosts(numberOfGhosts)
	{
		var highscores = JSON.parse(data.gHighscoresAsString());
		
		if(highscores == null)
		{
			return null;
		}
		
		var highscoresForNumberOfGhosts = highscores["highscores"][numberOfGhosts];
		
		if(highscoresForNumberOfGhosts == null)
		{
			return null;
		}
		
		return highscoresForNumberOfGhosts;
	}
	
	return {init: init, adaptSize : adaptSize, tabbingClick : tabbingClick};
})();

window.onload = menuHighscores.init;
window.onresize = menuHighscores.adaptSize;