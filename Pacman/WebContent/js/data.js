var data = (function() {
	var kH = "pcmn_highscores";
	var kG = "pcmn_numberOfGhosts";

	return {
		gHighscoresAsString : function() {return localStorage.getItem(kH);},
		sHighscoresAsString : function(val) {localStorage.setItem(kH, val);},
		gNumberOfGhosts : function() {return localStorage.getItem(kG);},
		sNumberOfGhosts : function(val) {localStorage.setItem(kG, val);}
	};
})();