var pauseMenu = (function() {
	
	function showPauseMenu() 
	{
		adaptSize();
		
		var overlay = document.getElementById('pauseOverlay');
		
		if(overlay.style.display == "block")
		{
			overlay.style.display = "none";
		} 
		else 
		{
			overlay.style.display = "block";
		}
	}
	
	function adaptSize()
	{
		var height = window.innerHeight;
		var width = window.innerWidth;

		var header = document.getElementById('header');
		var content = document.getElementById('content');
		var titleTextH1 = document.getElementById('titleText').getElementsByTagName('h1')[0];
		var menuNavigation = document.getElementById('navigationPause');
		var menuNavigationAs = menuNavigation.getElementsByTagName('a');
		
		header.style.height = height * 0.45 + "px";
		content.style.height = height * 0.55 + "px";
		
		forEach.call(menuNavigationAs, function(e)
		{
			e.style.marginBottom = height * 0.02 + "px";
			e.style.paddingTop = height * 0.03 + "px";
			e.style.paddingBottom = height * 0.03 + "px";
		});
		
		if(height < width)
		{
			titleTextH1.style.fontSize = height * 0.12 + "px";
			titleTextH1.style.paddingTop = height * 0.035 + "px";
			
			menuNavigation.style.fontSize = height * 0.06 + "px";
			
			forEach.call(menuNavigationAs, function(e)
			{
				e.style.fontSize = height * 0.06 + "px";
			});
		}
		else
		{
			titleTextH1.style.fontSize = width * 0.12 + "px";
			titleTextH1.style.paddingTop = width * 0.035 + "px";
			
			menuNavigation.style.fontSize = width * 0.06 + "px";
			
			forEach.call(menuNavigationAs, function(e)
			{
				e.style.fontSize = width * 0.06 + "px";
			});
		}
		
		menuNavigation.style.width = width * 0.75 + "px";
		
		menuNavigation.style.paddingTop = (content.offsetHeight / 4) + "px";
		menuNavigation.style.paddingLeft = ((width - menuNavigation.style.width.replace("px", "")) / 2) + "px";
		
		titleTextH1.style.paddingBottom = height * 0.1 + "px";
	}
	
	function continueGame()
	{
		var overlay = document.getElementById('pauseOverlay');
		overlay.style.display = "none";
		game.changePauseState();
	}
	
	return { showPauseMenu : showPauseMenu, continueGame : continueGame };
})();