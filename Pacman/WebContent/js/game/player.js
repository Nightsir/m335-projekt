var player  = (function(){
	var x;
	var y;
	
	var oX;
	var oY;
	
	var curDir;
	
	function isDirectionPossible(dir) {
		var possible;
		
		switch (dir) {
			case direction.up:
				possible = (field.getField()[y - 1][x].match(/[* ]/) != null);
				break;
			case direction.right:
				possible = (field.getField()[y][x + 1].match(/[* ]/) != null);
				break;
			case direction.down:
				possible = (field.getField()[y + 1][x].match(/[* ]/) != null);
				break;
			case direction.left:
				possible = (field.getField()[y][x - 1].match(/[* ]/) != null);
				break;
		}
		
		return possible;
	}
	
	function move() {
		if(isDirectionPossible(curDir)) {
			field.getField()[y][x] = " ";
			
			switch (curDir) {
				case direction.up:
					y--;
					break;
				case direction.right:
					x++;
					break;
				case direction.down:
					y++;
					break;
				case direction.left:
					x--;
					break;
			}
			
			if(field.getField()[y][x] == "*") {
				score.increment();
			}
			field.getField()[y][x] = "p";
		}
	}
	
	function reset() {
		field.getField()[y][x] = " ";
		
		x = oX;
		y = oY;
		
		field.getField()[y][x] = "p";
	}
	
	return {
		gX : function() {return x;},
		gY : function() {return y;},
		gCurDir : function() {return curDir},
		set : function(sx, sy) {x = sx; y = sy; oX = sx; oY = sy;},
		sCurDir : function(nDir) {curDir = nDir},
		move : move,
		isDirectionPossible : isDirectionPossible,
		reset : reset
	};
})();