var enemy = (function() {
	var g1 = (function() {
		var x;
		var y;
		var oX;
		var oY;
		var dir;
		
		return {
			gX : function() {return x;},
			sX : function(nx) {x = nx},
			gY : function() {return y;},
			sY : function(ny) {y = ny},
			sOX : function(nx) {oX = nx},
			sOY : function(ny) {oY = ny},
			reset : function() {x = oX; y = oY},
			gDir : function() {return dir;},
			sDir : function(ndir) {dir = ndir}
		};
	})();
	
	var g2 = (function() {
		var x;
		var y;
		var oX;
		var oY;
		var dir;
		
		return {
			gX : function() {return x;},
			sX : function(nx) {x = nx},
			gY : function() {return y;},
			sY : function(ny) {y = ny},
			sOX : function(nx) {oX = nx},
			sOY : function(ny) {oY = ny},
			reset : function() {x = oX; y = oY},
			gDir : function() {return dir;},
			sDir : function(ndir) {dir = ndir}
		};
	})();
	
	var g3 = (function() {
		var x;
		var y;
		var oX;
		var oY;
		var dir;
		
		return {
			gX : function() {return x;},
			sX : function(nx) {x = nx},
			gY : function() {return y;},
			sY : function(ny) {y = ny},
			sOX : function(nx) {oX = nx},
			sOY : function(ny) {oY = ny},
			reset : function() {x = oX; y = oY},
			gDir : function() {return dir;},
			sDir : function(ndir) {dir = ndir}
		};
	})();
	
	var g4 = (function() {
		var x;
		var y;
		var oX;
		var oY;
		var dir;
		
		return {
			gX : function() {return x;},
			sX : function(nx) {x = nx},
			gY : function() {return y;},
			sY : function(ny) {y = ny},
			sOX : function(nx) {oX = nx},
			sOY : function(ny) {oY = ny},
			reset : function() {x = oX; y = oY},
			gDir : function() {return dir;},
			sDir : function(ndir) {dir = ndir}
		};
	})();
	
	function isDirectionPossible(e, dir) {
		var x = e.gX();
		var y = e.gY();
		
		var possible;
		
		switch (dir) {
			case direction.up:
				possible = (field.getField()[y - 1][x].match(/[*p ]/) != null);
				break;
			case direction.right:
				possible = (field.getField()[y][x + 1].match(/[*p ]/) != null);
				break;
			case direction.down:
				possible = (field.getField()[y + 1][x].match(/[*p ]/) != null);
				break;
			case direction.left:
				possible = (field.getField()[y][x - 1].match(/[*p ]/) != null);
				break;
		}
		
		return possible;
	}
	
	function areOtherPossible(e) {
		var possible = false;
		
		switch (e.gDir()) {
			case direction.up:
			case direction.down:
				possible = isDirectionPossible(e, direction.right) || isDirectionPossible(e, direction.left);
				break;
			case direction.right:
			case direction.left:
				possible = isDirectionPossible(e, direction.up) || isDirectionPossible(e, direction.down);
				break;
		}
		
		return possible;
	}
	
	function getAllPossibleDir(e) {
		var ret = [];
		
		if(isDirectionPossible(e, direction.up)) {
			ret.push(direction.up);
		}
		if(isDirectionPossible(e, direction.right)) {
			ret.push(direction.right);
		}
		if(isDirectionPossible(e, direction.down)) {
			ret.push(direction.down);
		}
		if(isDirectionPossible(e, direction.left)) {
			ret.push(direction.left);
		}
		
		return ret;
	}
	
	function move(e) {
		switch (e.gDir()) {
			case direction.up:
				e.sY(e.gY() - 1);
				break;
			case direction.right:
				e.sX(e.gX() + 1);
				break;
			case direction.down:
				e.sY(e.gY() + 1);
				break;
			case direction.left:
				e.sX(e.gX() - 1);
				break;
		}
	}
	
	function checkIfGhostOnPlayer() {
		var decrease = false;
		
		switch (data.gNumberOfGhosts() * 1) {
			case 4:
				decrease = decrease || field.getField()[g4.gY()][g4.gX()] == "p";
			case 3:
				decrease = decrease || field.getField()[g3.gY()][g3.gX()] == "p";
			case 2:
				decrease = decrease || field.getField()[g2.gY()][g2.gX()] == "p";
			case 1:
				decrease = decrease || field.getField()[g1.gY()][g1.gX()] == "p";
		}
		
		if(decrease) {
			game.decreaseLife();
		}
	}
	
	return {
		g1 : g1,
		g2 : g2,
		g3 : g3,
		g4 : g4,
		isOwnDirectionPossible : function(e) {return isDirectionPossible(e, e.gDir());},
		areOtherPossible : areOtherPossible,
		isDirectionPossible : isDirectionPossible,
		getAllPossibleDirs : getAllPossibleDir,
		move : move,
		checkIfGhostOnPlayer : checkIfGhostOnPlayer,
		resetGhosts : function() {g1.reset(); g2.reset(); g3.reset(); g4.reset();}
	};
})();