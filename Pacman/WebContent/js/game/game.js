var game = (function(){
	var gameCycleInterval;
	
	var scoreElement;
	var pauseElement;
	var fieldElement;
	
	var g1Img;
	var g2Img;
	var g3Img;
	var g4Img;
	var pacman;
	var point;
	
	var ghosts;
	
	var paused;
	var level;
	var countdown;
	
	var spaceHorizontal;
	var spaceVertical;
	var tileWidth;
	var tileHeight;
	
	function init() {
		field.initLandscape();
		
		scoreElement = document.getElementById("score");
		pauseElement = document.getElementById("pause");
		fieldElement = document.getElementById("field");
		
		g1Img = document.getElementById("g1");
		g2Img = document.getElementById("g2");
		g3Img = document.getElementById("g3");
		g4Img = document.getElementById("g4");
		pacman = document.getElementById("pacman");
		point = document.getElementById("point");
		
		ghosts = data.gNumberOfGhosts() * 1;
		
		paused = false;
		level = 1;
		countdown = 6;
		score.reset();
		
		window.addEventListener("deviceorientation", orientationEventHandler, false);
		window.addEventListener("keydown", keyEventHandler);
		
		size();
		
		repaint();
		paintReady();
		
		game.startGameCycle();
	}
	
	function size() {
		fieldElement.width = window.innerWidth;
		fieldElement.height = (window.innerHeight - document.getElementById("head").offsetHeight - 5);
		fieldElement.style.width = window.innerWidth;
		fieldElement.style.height = (window.innerHeight - document.getElementById("head").offsetHeight - 5) + "px";
		
		spaceHorizontal = fieldElement.width % field.getField()[0].length;
		spaceVertical = fieldElement.height % field.getField().length;
		
		tileWidth = (fieldElement.width - spaceHorizontal) / field.getField()[0].length;
		tileHeight = (fieldElement.height - spaceVertical) / field.getField().length;
		
		spaceHorizontal = (spaceHorizontal - spaceHorizontal % 2) / 2;
		spaceVertical = (spaceVertical - spaceVertical % 2) / 2;
	}
	
	function decreaseLife() {
		var hearts = document.getElementById("hearts").getElementsByTagName("img");
		
		if(hearts.length > 1) {
			document.getElementById("hearts").removeChild(hearts[hearts.length - 1]);
			
			enemy.resetGhosts();
			player.reset();
			
			countdown = 6;

			repaint();
			paintReady();
		} else {
			game.stopGameCycle();
			score.saveScore();
			
			window.location.href = "gameover.html";
		}
	}
	
	function writeHead() {
		if(window.innerWidth < window.innerHeight) {
			scoreElement.innerHTML = score.gScore();
			pauseElement.innerHTML = "II";
		} else {
			scoreElement.innerHTML = "Score: " + score.gScore();
			pauseElement.innerHTML = "Pause II";
		}
	}
	
	function fillAt(ctx, color, x, y) {
		ctx.fillStyle = color;
		ctx.fillRect(x * tileWidth + spaceHorizontal, y * tileHeight + spaceVertical, tileWidth, tileHeight);
	}
	
	function drawImageAt(ctx, img, x, y) {
		ctx.drawImage(img, x * tileWidth + spaceHorizontal, y * tileHeight + spaceVertical, tileWidth, tileHeight);
	}
	
	function paintReady() {
		var ctx = fieldElement.getContext("2d");
		
		ctx.font="30px Arial";
		ctx.textAlign="center";
		
		var text = "LEVEL " + level;
		var width = ctx.measureText(text).width;

		ctx.fillStyle = "#FFFFFF";
		ctx.fillRect(fieldElement.width / 2 - width / 2 - 10, fieldElement.height / 2 - 35, width + 20, 50);
		
		ctx.fillStyle = "#000000";
		ctx.fillText(text, fieldElement.width / 2, fieldElement.height / 2);
	}
	
	function repaint() {
		writeHead();
		
		var ctx = fieldElement.getContext("2d");
		
		var drawField = field.getField();
		
		for(var y = 0; y < drawField.length; y++) {
			for(var x = 0; x < drawField[y].length; x++) {
				fillAt(ctx, "#000000", x, y);
				switch (drawField[y][x]) {
					case "w":
						fillAt(ctx, "#3535FF", x, y);
						break;
					case "*":
						drawImageAt(ctx, point, x, y);
						break;
					case "p":
						var angle;
						
						switch (player.gCurDir()) {
							case direction.right:
								angle = 0;
								break;
							case direction.down:
								angle = 90;
								break;
							case direction.left:
								angle = 180;
								break;
							case direction.up:
								angle = 270;
								break;
						}
						
						ctx.save();
						ctx.translate(x * tileWidth + spaceHorizontal + tileWidth / 2, y * tileHeight + spaceVertical + tileHeight / 2);
						ctx.rotate(angle * Math.PI / 180);
						ctx.translate(-(x * tileWidth + spaceHorizontal + tileWidth / 2), -(y * tileHeight + spaceVertical + tileHeight / 2));
						drawImageAt(ctx, pacman, x, y);
						ctx.restore();
						break;
					default:
						break;
				}
			}
		}
		
		switch (ghosts) {
			case 4:
				drawImageAt(ctx, g4Img, enemy.g4.gX(), enemy.g4.gY());
			case 3:
				drawImageAt(ctx, g3Img, enemy.g3.gX(), enemy.g3.gY());
			case 2:
				drawImageAt(ctx, g2Img, enemy.g2.gX(), enemy.g2.gY());
			case 1:
				drawImageAt(ctx, g1Img, enemy.g1.gX(), enemy.g1.gY());
			default:
				break;
		}
		
	}
	
	function gameCycle() {
		if(!paused) {
			if(countdown > 0) {
				countdown--;
			} else {
				switch (ghosts) {
					case 4:
						if(enemy.areOtherPossible(enemy.g4)) {
							enemy.g4.sDir(direction.getRandomOfPossible(enemy.getAllPossibleDirs(enemy.g4)));
							enemy.move(enemy.g4);
						} else if(enemy.isOwnDirectionPossible(enemy.g4)) {
							enemy.move(enemy.g4);
						} else if(enemy.isDirectionPossible(enemy.g4, direction.getOpositeDir(enemy.g4.gDir()))) {
							enemy.g4.sDir(direction.getOpositeDir(enemy.g4.gDir()));
							enemy.move(enemy.g4);
						}
					case 3:
						if(enemy.areOtherPossible(enemy.g3)) {
							enemy.g3.sDir(direction.getRandomOfPossible(enemy.getAllPossibleDirs(enemy.g3)));
							enemy.move(enemy.g3);
						} else if(enemy.isOwnDirectionPossible(enemy.g3)) {
							enemy.move(enemy.g3);
						} else if(enemy.isDirectionPossible(enemy.g3, direction.getOpositeDir(enemy.g3.gDir()))) {
							enemy.g3.sDir(direction.getOpositeDir(enemy.g3.gDir()));
							enemy.move(enemy.g3);
						}
					case 2:
						if(enemy.areOtherPossible(enemy.g2)) {
							enemy.g2.sDir(direction.getRandomOfPossible(enemy.getAllPossibleDirs(enemy.g2)));
							enemy.move(enemy.g2);
						} else if(enemy.isOwnDirectionPossible(enemy.g2)) {
							enemy.move(enemy.g2);
						} else if(enemy.isDirectionPossible(enemy.g2, direction.getOpositeDir(enemy.g2.gDir()))) {
							enemy.g2.sDir(direction.getOpositeDir(enemy.g2.gDir()));
							enemy.move(enemy.g2);
						}
					case 1:
						if(enemy.areOtherPossible(enemy.g1)) {
							enemy.g1.sDir(direction.getRandomOfPossible(enemy.getAllPossibleDirs(enemy.g1)));
							enemy.move(enemy.g1);
						} else if(enemy.isOwnDirectionPossible(enemy.g1)) {
							enemy.move(enemy.g1);
						} else if(enemy.isDirectionPossible(enemy.g1, direction.getOpositeDir(enemy.g1.gDir()))) {
							enemy.g1.sDir(direction.getOpositeDir(enemy.g1.gDir()));
							enemy.move(enemy.g1);
						}
						break;
				}
				
				player.move();
				
				repaint();
				
				enemy.checkIfGhostOnPlayer();
				
				if(field.isEmpty()) {
					level++;
					field.initLandscape();
					
					countdown = 6;
					
					repaint();
					paintReady();
				}
			}
		}
	}
	
	function orientationEventHandler(eventData) {
		var ud = Math.round(eventData.gamma / 10);
		var lr = Math.round(eventData.beta / 10);
		
		if(Math.abs(ud) > Math.abs(lr)) {
			if(ud < 0) {
				if(player.isDirectionPossible(direction.down)) {
					player.sCurDir(direction.down);
				}
			} else {
				if(player.isDirectionPossible(direction.up)) {
					player.sCurDir(direction.up);
				}
			}
		} else if(Math.abs(ud) < Math.abs(lr)) {
			if(lr <  0) {
				if(player.isDirectionPossible(direction.left)) {
					player.sCurDir(direction.left);
				}
			} else {
				if(player.isDirectionPossible(direction.right)) {
					player.sCurDir(direction.right);
				}
			}
		}
	}
	
	//handler for testing in browser
	function keyEventHandler(event) {
		switch (event.keyCode) {
			case 38:
				if(player.isDirectionPossible(direction.up)) {
					player.sCurDir(direction.up);
				}
				break;
			case 39:
				if(player.isDirectionPossible(direction.right)) {
					player.sCurDir(direction.right);
				}
				break;
			case 40:
				if(player.isDirectionPossible(direction.down)) {
					player.sCurDir(direction.down);
				}
				break;
			case 37:
				if(player.isDirectionPossible(direction.left)) {
					player.sCurDir(direction.left);
				}
				break;
		}
	}
	
	function changePauseState() {
		paused = !paused;
		
		if(paused) {
			pauseMenu.showPauseMenu();
		}
	}

	return{
		init : init,
		changePauseState : changePauseState,
		startGameCycle : function() {gameCycleInterval = window.setInterval(gameCycle, 250);},
		stopGameCycle : function() {window.clearInterval(gameCycleInterval);},
		gLevel : function() {return level;},
		decreaseLife : decreaseLife
	};
})();

window.onload = game.init;
