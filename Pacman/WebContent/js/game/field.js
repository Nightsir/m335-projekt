var field = (function(){
	var fieldLandscape = [
		["w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w"],
		["w","*","*","*","*","*","*","*","*","*","*","*","w","*","*","*","*","*","*","*","*","*","*","*","w"],
		["w","*","w","w","w","w","*","w","w","w","w","*","w","*","w","w","w","w","*","w","w","w","w","*","w"],
		["w","*","w","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","w","*","w"],
		["w","*","*","*","w","w","*","w","*","w","w","w","w","w","w","w","*","w","*","w","w","*","*","*","w"],
		["w","*","w","*","*","*","*","w","*","*","*","*","w","*","*","*","*","w","*","*","*","*","w","*","w"],
		["w","*","w","w","w","w","*","w","w","w","w","*","w","*","w","w","w","w","*","w","w","w","w","*","w"],
		["w","*","*","*","*","*","*","w"," "," "," "," ","4"," "," "," "," ","w","*","*","*","*","*","*","w"],
		["w","w","w","w","w","w","*","w"," ","w","w","w"," ","w","w","w"," ","w","*","w","w","w","w","w","w"],
		["w","*","*","*","*","*","*","*"," ","w","3"," ","1"," ","2","w"," ","*","*","*","*","*","*","*","w"],
		["w","*","w","*","w","w","w","w"," ","w","w","w","w","w","w","w"," ","w","w","w","w","*","w","*","w"],
		["w","*","w","*","*","*","*","*"," "," "," "," ","p"," "," "," "," ","*","*","*","*","*","w","*","w"],
		["w","*","w","w","w","w","w","w","*","w","w","w","w","w","w","w","*","w","w","w","w","w","w","*","w"],
		["w","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","*","w"],
		["w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w","w"]];
	
	var fieldArr;
	
	var possiblePoints;
	
	function initLandscape() {
		fieldArr = [];
		possiblePoints = 0;
		
		var ghosts = data.gNumberOfGhosts() * 1;
		
		for(var y = 0; y < fieldLandscape.length; y++) {
			var tmp = [];
			for(var x = 0; x < fieldLandscape[y].length; x++) {
				if((fieldLandscape[y][x] * 1) > ghosts) {
					tmp.push(" ");
				} else {
					switch (fieldLandscape[y][x] * 1) {
						case 1:
							enemy.g1.sX(x);
							enemy.g1.sY(y);
							enemy.g1.sOX(x);
							enemy.g1.sOY(y);
							enemy.g1.sDir(direction.up);
							tmp.push(" ");
							break;
						case 2:
							enemy.g2.sX(x);
							enemy.g2.sY(y);
							enemy.g2.sOX(x);
							enemy.g2.sOY(y);
							enemy.g2.sDir(direction.left);
							tmp.push(" ");
							break;
						case 3:
							enemy.g3.sX(x);
							enemy.g3.sY(y);
							enemy.g3.sOX(x);
							enemy.g3.sOY(y);
							enemy.g3.sDir(direction.right);
							tmp.push(" ");
							break;
						case 4:
							enemy.g4.sX(x);
							enemy.g4.sY(y);
							enemy.g4.sOX(x);
							enemy.g4.sOY(y);
							enemy.g4.sDir(direction.right);
							tmp.push(" ");
							break;
					}
					
					if(fieldLandscape[y][x] == "p") {
						player.set(x, y);
						player.sCurDir(direction.right);
					} else if(fieldLandscape[y][x] == "*") {
						possiblePoints++;
					}
					
					if(tmp.length == x) {
						tmp.push(fieldLandscape[y][x]);
					}
				}
			}
			fieldArr.push(tmp);
		}
	}
	
	return {
		initLandscape : initLandscape,
		getField : function() {return fieldArr;},
		isEmpty : function() {return possiblePoints == 0;},
		decreasePossiblePoints : function() {possiblePoints--;}
	};
})();