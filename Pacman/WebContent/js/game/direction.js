var direction = (function(){
	var up = 0;
	var right = 1;
	var down = 2;
	var left = 3;
	
	function rand() {
		return Math.floor(Math.random() * 4);
	}
	
	function getRandomOfPossible(possibilities) {
		return possibilities[Math.floor(Math.random() * possibilities.length)];
	}
	
	function getOpositeDir(dir) {
		var retDir;
		
		switch (dir) {
			case up:
				retDir = down;
				break;
			case right:
				retDir = left;
				break;
			case down:
				retDir = up;
				break;
			case left:
				retDir = right;
				break;
		}
		
		return retDir;
	}
	
	return {
		up : up,
		right : right,
		down : down,
		left : left,
		getRandom : rand,
		getRandomOfPossible : getRandomOfPossible,
		getOpositeDir : getOpositeDir
	};
})();