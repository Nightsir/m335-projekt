var score = (function() {
	var points;
	
	function savePoints() {
		var scoresObj = JSON.parse(data.gHighscoresAsString());
		var nrGhosts = data.gNumberOfGhosts();
		
		if(scoresObj == null) {
			scoresObj = {
				highscores : {}
			}
		}
		
		if(scoresObj["highscores"][nrGhosts] == undefined) {
			scoresObj["highscores"][nrGhosts] = [0, 0, 0, 0, 0];
		}
		
		scoresObj["highscores"][nrGhosts].push(points);
		scoresObj["highscores"][nrGhosts].sort(function(a, b){return b-a});
		
		scoresObj["highscores"][nrGhosts].splice(scoresObj["highscores"][nrGhosts].length - 1, 1);
		
		data.sHighscoresAsString(JSON.stringify(scoresObj));
	}
	
	return {
		gScore : function() {return points;},
		reset : function() {points = 0;},
		increment : function() {points+=game.gLevel(); field.decreasePossiblePoints();},
		saveScore : savePoints
	};
})();